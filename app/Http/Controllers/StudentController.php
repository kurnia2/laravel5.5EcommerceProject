<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $name = 'Md. Monirul Islam';
        // 1. return view('home', compact('name'));
        // 2. return view('home')->with('n', $name);
        return view('home', ['name'=>$name]);
    }
    public function add()
    {
        return 'i am from';
    }
}
